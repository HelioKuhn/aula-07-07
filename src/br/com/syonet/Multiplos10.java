/**
 * 6) Múltiplos de 10: Dado array,
 *  você deve avaliar cada posição. 
 *  Quando encontrar um múltiplo de 10,
 *   você deve substituir os próximos valores por esse múltiplo. 
 *   Caso encontre outro, o valor deve mudar para este novo múltiplo.
 *  Ex: 1, 10, 11, 20, 12 -> 1, 10, 10, 20, 20.
 */

package br.com.syonet;

import java.util.Scanner;

public class Multiplos10 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite o tamanho do array: ");
		int tamanhoArray = sc.nextInt();

		int[] arrayNumeros = new int[tamanhoArray];

		System.out.print("Digite os números do array: ");
		for (int i = 0; i < tamanhoArray; i++) {
			arrayNumeros[i] = sc.nextInt();
		}
		
		int[] novoArray = new int[tamanhoArray];
		for(int i=0; i< arrayNumeros.length; i++) {
			if(arrayNumeros[i] %10==0 && i < arrayNumeros.length) {
				novoArray[i] = arrayNumeros[i];
				novoArray[i+1] = arrayNumeros[i];
				i++;
			}else {
				novoArray[i] = arrayNumeros[i];
				
			}
		}
		
		System.out.println("Saída do array com números multiplos de 10: ");
		for(int n: novoArray) {
			System.out.print(n+ ", ");
		}
		
		sc.close();
		
	}

}

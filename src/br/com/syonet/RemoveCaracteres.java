/**
 * 3) Remoção de caracteres: Dado um texto,
 *  vocês devem remover as vogais dele e retornar o texto sem estes caracteres;
 */
package br.com.syonet;

public class RemoveCaracteres {

	public static void main(String[] args) {

		String texto = "Texto que será removidO as vogais";

		String textoNovo = texto.replaceAll("[aeiouAEIOUáàéèíóõ]", "");

		System.out.println(textoNovo);

		System.out.println("-------------------");
		System.out.println(removerCaracteres("Texto para ser removidas as vogais"));
		
		System.out.println("-------------------------------");
		
		System.out.println(removerCaracteres2("exto para ser removidas as vog"));
		
		System.out.println(removerCaracteres3("Texto para ser removido as vogais"));

	}

	
	//Sem regex
	static String removerCaracteres(String texto) {
		String novoTexto = "";
		for (int i = 0; i < texto.length(); i++) {
			
			if(texto.charAt(i) != 'a' && texto.charAt(i) != 'e' && texto.charAt(i) != 'i' && texto.charAt(i) != 'o' && texto.charAt(i) != 'u' && texto.charAt(i) != 'A' && texto.charAt(i) != 'E' && texto.charAt(i) != 'I' &&  texto.charAt(i) != 'O' && texto.charAt(i) != 'U' ) {
				novoTexto += texto.charAt(i);
			}

		}
		return novoTexto;
	}
	

	
	//Terceira forma
	static String removerCaracteres2(String texto) {
		String novoTexto = "";
		for (int i = 0; i < texto.length(); i++) {
			
			if(!isVogal(texto.charAt(i))) {
				novoTexto +=texto.charAt(i);
			}

		}
		return novoTexto;
	}
	
	static boolean isVogal(char caractere) {
		return (caractere == 'a'|| caractere == 'e'||caractere == 'i'||caractere ==  'o'||caractere ==  'u'||
				caractere ==  'A'|| caractere ==  'E'|| caractere ==  'I'|| caractere ==  'O'|| caractere ==  'U');
	}
	
	static String removerCaracteres3(String texto) {
		String novoTexto = "";
		char[] vogais = {'a', 'e', 'i', 'o', 'u'};
		
		for (int i = 0; i < texto.length(); i++) {
			boolean isvogal = false;
			for (int j=0; j< vogais.length; j++) {
				if(texto.charAt(i) == vogais[j]) {
					isvogal = true;
				}
			}
			if(!isvogal) {
				novoTexto+=texto.charAt(i);
				isvogal = false;
			}
		}
		return novoTexto;
	}

}

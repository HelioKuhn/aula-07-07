/**
 * 2 )  Soma: Dado determinado número, vocês devem efetuar a soma de 0
 *  até o número informado conforme o nome do método.
 * Par: Apenas a soma de números pares.
 * Impar: Apenas a soma de números ímpares.
 */

package br.com.syonet;

import java.util.Scanner;

public class Soma {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite um número para ser somado os números pares e ímpares: ");
		
		int numero = sc.nextInt();
		
		int totalPar = somarPar(numero);
		int totalImpares = somarImpar(numero);
		
		System.out.printf("O total de números pares encontrados de 0 até o número %d é de %d%n", numero, totalPar);
		System.out.printf("O total de números impares encontrados de 0 até o número %d é de %d", numero, totalImpares);
		sc.close();
	}
	
	
	
	static int somarPar(int numero) {
		
		int pares = 0;
		for(int i = 0; i<= numero; i++) {
			if(i%2!=0) {
				continue;
			}
			else {
				pares ++;
			}

		}
		return pares;
	}
	
	static int somarImpar(int numero) {
		int impares = 0;
		for(int i = 0; i<= numero; i++) {
			if(i%2==0) {
				continue;
			}
			else {
				impares++;
			}
		}
		return impares;
	}
}

/**
 * 1 ) Extrato: Na empresa em que trabalhamos, há tabelas com o gasto de cada mês. 
 * Para fechar o balanço do primeiro trimestre, precisamos somar o gasto total. 
 * Sabendo que, em janeiro, foram gastos 15 mil reais, em fevereiro, 23 mil reais e, em março, 17 mil reais,
 *  faça uma classe que calcule e imprima a despesa total no trimestre e a média mensal de gastos.
 */
		

package br.com.syonet;

public class Extrato {

	public static void main(String[] args) {
		float gastosJaneiro = 15000f;
		float gastosFevereito = 23000f;
		float gastosMarco = 17000f;
		
		float totalTrimestre = gastosJaneiro + gastosFevereito + gastosMarco;
		
		System.out.printf("O total das despesas do trimestre é de R$%.2f, e o custo médio mensal é de R$%.2f", totalTrimestre, totalTrimestre/3.0);

	}

}

/**
 * 4) Primos: Dado um número, verifique e retorne se o número informado é primo.
 */

package br.com.syonet;

import java.util.Scanner;

public class NumeroPrimo {
	
	public static void main(String[] args) {
		
		System.out.print("Digite um número para verificar se ele é primo: ");
		
		
		Scanner sc = new Scanner(System.in);
		
		int numero = sc.nextInt();
		
		System.out.println(isPrimo(numero));
			
		sc.close();
		
	}
	
	
	static boolean isPrimo(int numero) {
		return (numero%2!=0&&numero%3!=0&&numero!=1&&numero%5!=0)||numero==2||numero==3||numero==5;
	}
}

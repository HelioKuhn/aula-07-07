/**
 * 5) Apenas 1 e 4: Você deve fazer um método que avalia o array passado e diz se ele só contém 1 e 4 :)
 */
package br.com.syonet;

import java.util.Scanner;

public class Apenas1e4 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite o tamanho do array: ");
		
		int tamanhoArray = sc.nextInt();
		
		int[]arrayNumeros = new int[tamanhoArray];
		
		System.out.print("Digite os números do array: ");
		for(int i=0; i<tamanhoArray; i++) {
			arrayNumeros[i] = sc.nextInt();
		}
		
		boolean validarNumeros = true;
		
		for(int i =0; i<arrayNumeros.length; i++) {
			if(arrayNumeros[i] != 1 && arrayNumeros[i] != 4) {
				validarNumeros = false;
				break;
			}	
		}
		
		
		if(validarNumeros) {
			System.out.println("Array só possue números 1 ou 4");
		}else System.out.println("Array possue números diferentes de 1 ou 4");
		
		System.out.println("------------------");

		
		
		
		System.out.println("Chamando por método");
		if(somenteUmEQuatro(arrayNumeros)) {
			System.out.println("Array possue só números 1 e 4");
		}else System.out.println("Array possue números diferentes de 1 ou 4");
		sc.close();
		
	}
	
	static boolean somenteUmEQuatro(int[] array){
		for(int i =0; i<array.length; i++) {
			if(array[i] != 1 && array[i] != 4) {
				return false;
			}
			
		}
		return true;
	}
	

}
